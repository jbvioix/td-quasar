export function addContact(state, _contact) {
    state.contacts.push({
        nom: _contact.nom,
        prenom: _contact.prenom,
        email: _contact.email,
        telephone: _contact.telephone
    })
}

export function clearAll(state) {
    state.contacts.length = 0;
}

export function deleteContact(state, _id) {
    state.contacts.splice(_id, 1);
}

export function updateContact(state, payload) {
    console.log("updateContact", payload)
    state.contacts[payload.id] = {
        nom: payload.nom,
        prenom: payload.prenom,
        email: payload.email,
        telephone: payload.telephone
    }
}