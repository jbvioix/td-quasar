export function getAll (state) {
    return state.contacts
}

export function getById (state) {
    return (id) => {
        return state.contacts[id]
    }
}
