
const routes = [
  {
    path: '/nouveau/',
    name: 'Nouveau',
    component: () => import('src/pages/Nouveau.vue'),
    // children: [
    //   { path: '', component: () => import('components/ContactForm.vue') }
    // ]

  },
  {
    path: '/editer/:id',
    name: 'Editer',
    component: () => import('src/pages/Edition.vue'),
    props: true
    // children: [
    //   { path: '', component: () => import('components/ContactForm.vue') }
    // ]

  },

  


  {
    path: '/',
    // name: "Accueil",
    component: () => import('pages/Index.vue'),
  },
  

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }

]

export default routes
